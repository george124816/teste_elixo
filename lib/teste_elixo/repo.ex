defmodule TesteElixo.Repo do
  use Ecto.Repo,
    otp_app: :teste_elixo,
    adapter: Ecto.Adapters.Postgres
end
