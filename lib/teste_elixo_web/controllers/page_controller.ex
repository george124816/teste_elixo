defmodule TesteElixoWeb.PageController do
  use TesteElixoWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
